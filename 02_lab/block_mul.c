#include <stdio.h>
#include <omp.h>

#define N 200
#define B 10

int min(int a,int b)
{
	if (a<=b)
	return a;
	else
	return b;
}

int main()
{
	long double x[N][N] = {0};
	long double y[N][N] = {1};
	long double z[N][N] = {0};

	long double start, stop,total;
	//int arg = atoi(argv[1]);
	int i,j,k,ii,jj,kk;
	int threads[] = {1, 2, 4, 6, 8, 10, 12, 14, 18, 22, 26, 30, 34, 38, 42, 46, 50, 54, 58, 62};
	int r;

	for (int var = 0; var < 20; ++var)
	{	
		start = omp_get_wtime();
		#pragma omp parallel num_threads(threads[var])
  		#pragma omp parallel for default(none) private(i,j,k,kk,jj) shared(x,y,z,r)          
        	for (jj = 0; jj < N; jj = jj+B)
          		for (kk = 0; kk < N; kk = kk+B)
            		for (i = 0; i < N; i = i+1)
            			for (j = jj; j < min(jj+B,N); j = j+1)
             				{
             				 	r = 0;
             				 	for (k = kk; k < min(kk+B,N); k = k+1) 
             						r = r + y[i][k]*z[k][j];
             					
             					x[i][j] = x[i][j] + r;
             				}

		
		stop = omp_get_wtime();
		total = (stop - start);
		printf("%d\t%LF\n", threads[var],(long double)total);
	}

	return 0;
}
