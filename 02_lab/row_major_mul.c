#include <stdio.h>
#include <omp.h>

#define SIZE 200

int main()
{
	long double a[SIZE][SIZE] = {0};
	long double b[SIZE][SIZE] = {1};
	long double c[SIZE][SIZE] = {0};

	long double start, stop,total;
	//int arg = atoi(argv[1]);
	int i,j,k;
	int threads[] = {1, 2, 4, 6, 8, 10, 12, 14, 18, 22, 26, 30, 34, 38, 42, 46, 50, 54, 58, 62};
	
	for (int var = 0; var < 20; ++var)
	{	
		start = omp_get_wtime();
		#pragma omp parallel for num_threads(threads[var]) private(i,j,k) shared(a,b,c)	
		for (i = 0; i < SIZE ; ++i)
		{	
			for (j = 0; j < SIZE; ++j)
			{
				for (k= 0; k < SIZE; ++k)
					c[i][j] = a[i][j] + b[i][j];
			}
		}
		stop = omp_get_wtime();
		total = (stop - start);
		printf("%d\t%LF\n", threads[var],(long double)total);
	}

	return 0;
}
