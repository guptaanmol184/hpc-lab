#!/bin/bash

#PBS -N Hw

#PBS -q default

#PBS -l nodes=5:ppn=10

#PBS -j oe

#PBS -V


cd $PBS_O_WORKDIR/

mpirun -machinefile $PBS_NODEFILE -np 50 ./HW
